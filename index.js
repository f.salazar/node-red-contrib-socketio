module.exports = function (RED) {

	const { Server } = require("socket.io");

	function socketServerConfig(config) {
		RED.nodes.createNode(this, config);

		this.config = config;
		let optionList = {};
		let io;

		if (this.config.options.length > 0) {
			this.config.options.map((element, index) => {
				const option = element;
				delete option["type"];
				delete option["index"];
				Object.assign(optionList, element);
			})
		}

		const socketOptions = {
			path: this.config.path || "/socket",
			...optionList
		};

		if (this.config.useNodePort) {
			io = new Server(RED.server, {
				...socketOptions
			});
		} else {
			io = new Server(this.config.port, {
				...socketOptions
			});
		}

		const getStatus = (status, props) => {
			switch (status) {
				case "start":
					return {
						fill: "green",
						shape: "dot",
						text: `Listen on port: ${this.config.port}`
					}
					break;
				case "stop":
					return {
						fill: "gray",
						shape: "dot",
						text: `Server closed`
					}
					break;
				case "clientConnected":
					return {
						fill: "green",
						shape: "ring",
						text: `Client connected`
					}
					break;
				case "clientDisconnected":
					return {
						fill: "red",
						shape: "dot",
						text: `Client disconnected`
					}
					break;
				case "clientTraffic":
					return {
						fill: "blue",
						shape: "ring",
						text: `Client message`
					}
					break;
				default:
					return {
						fill: "grey",
						shape: "dot",
						text: `Status error`
					}
					break;
			}
		};

		const statusHandler = (event, status, statusProps = {}, prevStatus = undefined, timeout = 2000) => {
			this.emit(event, getStatus(status, statusProps));
			if (prevStatus) {
				setTimeout(() => {
					this.emit(event, getStatus(prevStatus, statusProps));
				}, timeout);
			}
		};

		io.on("connection", (socket) => {
			statusHandler("status", "clientConnected", "start");
			socket.on("message", (data) => {
				this.emit("message", data);
				statusHandler("status", "clientTraffic", "start");
			});
			socket.on("disconnect", (reason) => {
				this.emit("disconnect", ({ reason }));
				statusHandler("status", "clientDisconnected", "start");
			});
		})

		this.on("incomming-message", (data) => {
			const { event, args } = data;
			if (event && args) { io.emit(event, args) }
			else { this.emit("error", "event or args not found in msg, please pass event and args into msg for send") }
		});

		this.on("close", () => {
			io.disconnectSockets();
			io.close();
			this.emit("status", getStatus("stop"));
		});

		this.emit("status", getStatus("start", { port: this.port }));
	}

	function socketServer(config) {
		RED.nodes.createNode(this, config);

		this.server = RED.nodes.getNode(config.server);

		this.server.on("message", (msg) => {
			this.send(msg);
		});

		this.server.on("status", (status) => {
			this.status(status);
		});

		this.server.on("error", (msg) => {
			this.warn(msg);
		});

		this.on("input", (msg) => {
			this.server.emit("incomming-message", msg);
		});

	}

	RED.nodes.registerType("socketio-config", socketServerConfig);
	RED.nodes.registerType("socketio-server", socketServer);
}
